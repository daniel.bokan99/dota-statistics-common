package cmn

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/lib/pq"

	_ "github.com/lib/pq"
)

type Match struct {
	MatchId     int    `json:"match_id"`
	MatchSeqNum int    `json:"match_seq_num"`
	RadiantWin  bool   `json:"radiant_win"`
	StartTime   int    `json:"start_time"`
	Duration    int    `json:"duration"`
	RadiantTeam string `json:"radiant_team"`
	DireTeam    string `json:"dire_team"`
}

type Metadata struct {
	MatchCount         int    `json:"match_Count"`
	MinStartTime       int    `json:"min_startTime"`
	MaxStartTime       int    `json:"max_startTime"`
	MinDuration        int    `json:"min_duration"`
	MaxDuration        int    `json:"max_duration"`
	MinInsertTimestamp string `json:"min_insertTS"`
	MaxInsertTimestamp string `json:"max_insertTS"`
}

type Config struct {
	Host       string `json:"Host"`
	Port       int    `json:"Port"`
	User       string `json:"User"`
	Password   string `json:"Password"`
	Dbname     string `json:"Dbname"`
	Ip         string `json:Ip`
	ServerPort string `json:"ServerPort"`
}

var config Config

func SigInit(db *sql.DB, appId string, appStart time.Time, sqlInsert string) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)

	go func(db *sql.DB) {
		<-sigs
		_, err := db.Exec(sqlInsert, appId, appStart, time.Now())
		if err != nil {
			log.Println("Error while executing sql_appWorktimeQuery, err: ", err, time.Now())
		}
		db.Close()
		os.Exit(3)
	}(db)
}

func GetConfig() Config {
	return config
}

func DatabaseInit() (*sql.DB, error) {
	f, err := os.Open("config.json")
	if err != nil {
		log.Fatalln("Error while opening config file, err: ", err, time.Now())
	}
	err = json.NewDecoder(f).Decode(&config)
	if err != nil {
		log.Fatalln("Error while decoding json file, err: ", err, time.Now())
	}
	f.Close()

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", config.Host, config.Port, config.User, config.Password, config.Dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalln("Error while opening database, err: ", err, time.Now())
	}
	err = db.Ping()
	if err != nil {
		log.Fatalln("Error while pinging database, err: ", err, time.Now())
	}
	return db, err
}
